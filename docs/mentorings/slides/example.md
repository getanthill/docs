# Example

///

## Links

Here is a [link](https://getanthill.gitlab.io/docs/).

///

## Code highlighting

```ts
const a: String = "Hello";

console.log(a);
// Hello
```

///

## Quote


> He who controls the past controls the future; he who controls the present 
> controls the past."
> 
> *George Orwell’s novel 1984*
