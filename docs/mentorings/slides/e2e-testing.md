# e2e Testing

*Test your application the same way the users do.*










///

## Purpose

//

<img alt="" src="https://martinfowler.com/bliki/images/testPyramid/test-pyramid.png">

<footer>
<a href="https://martinfowler.com/bliki/TestPyramid.html">TestPyramid, Martin Fowler</a> / 
<a href="https://testing.googleblog.com/2015/04/just-say-no-to-more-end-to-end-tests.html">Just Say No to More End-to-End Tests, Mike Wacker</a>
</footer>

//

### Levels

User Interface (UI) <!-- .element: class="fragment"-->

Public API <!-- .element: class="fragment"-->

**-- STOP --** <!-- .element: class="fragment"-->

//

#### User Interface

Simulate the **exact** same interaction the users do with your app.

<footer>
Selenium, Cypress, Puppeteer
</footer>

//

#### (Public) API

Execute flows of requests in order.

<footer>
Postman, Newman, Gatling, JMeter or even Axios, etc.
</footer>










///

## Why

//

### Don't

Cost

Complexity <!-- .element: class="fragment"-->

Debugging <!-- .element: class="fragment"-->

Replace Unit Tests <!-- .element: class="fragment"-->

//

### Do

Non Regression <!-- .element: class="fragment"-->

Critical Paths <!-- .element: class="fragment"-->








///

## Rules to Win

//

### 1. Isolation

**Tests isolation** applied for e2e tests the same way than for
unit tests.

//

#### Scenario 1

Brute Force

```js

beforeEach(() => {
  db.drop();
  db.init();
  
  // Global configuration for the World:
  db.loadMinimalData();

  // Scoped defined uuid used in
  // every single data created:
  this.uuid = uuid();
});
```

Problems ? <!-- .element: class="fragment"-->

<footer class="fragment">
Must run in sequence, longer tests
</footer>

//

#### Scenario 2

World definition

```js

before(() => {
  db.drop();
  db.init();
  
  // Global configuration for the World:
  db.loadMinimalData();
});

beforeEach(() => {
  // Scoped defined uuid used in
  // every single data created:
  this.uuid = uuid();
});
```

<footer class="fragment">
Run in parallel within the same World
</footer>

//

#### Scenario 3

Parallel Worlds

```js
createWorld(uuid) {
  db.get(uuid)
    .drop();
    .init();
    .loadMinimalData();
}

before(() => Promise.all([
  createWorld('A')
  createWorld('B')
]));

describe('World A', () => null);
```

<footer>
Use of k8s namespaces
</footer>

//

### 2. Reproducible

Hunt Race Conditions

<div>Forget <code>wait</code></div> <!-- .element: class="fragment"-->

<div>Use fatal <code>timeout</code></div> <!-- .element: class="fragment"-->

<footer>
  Once a test case is passing, run it <strong>thousand times</strong> to check the test cases
  stability
</footer>

//

### 3. Avoid duplication

**SDK strategy** for internal use.

Used for Public API e2e tests first. <!-- .element: class="fragment"-->

Used then for the UI e2e data initialization. <!-- .element: class="fragment"-->

//

### 4. Assertions

<div><strong>UI</strong> Flows, Time Response, Visual Diffs</div> <!-- .element: class="fragment"-->

<div><strong>API</strong> Flows, Contracts, Responses</div> <!-- .element: class="fragment"-->

//

### 5. Keep It Simple

Factorize initialization code.

Focus on Critical Path: success and errors. <!-- .element: class="fragment"-->





///

## Environments

//

### Kubernetes cluster

Use namespaces to isolate test runs.

<footer>
Minikube for local testing
</footer>

//

### Instance

Might work on pretty big machines or small Tech Stacks.

<footer>
Local tests development can then be created over SSH tunnels.
</footer>

//

### Services

Databases such as `MongoDb` or `PostgreSQL` must be created and initialized on the fly.

<small>
Message brokers such as RabbitMQ must follow the same rule.
</small>

<footer>
<i>Why?</i> To always start from a clean and deterministic state
</footer>

//

### Logging

Use logs to debug tests the same way you would do in production

<footer>
Kibana, Dashboards, etc.
</footer>










///

## Methodology

//

### Bottom-Up

Always climb the testing pyramid

//

### Bottom-Up

Execute the tests in this order

//

### Continuous Testing

Run the tests periodically and frequently

<footer>
<i>Why?</i> Because fixing small regressions is possible while big once land 
in low priority
</footer>










///

## Questions
