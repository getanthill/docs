#!/usr/bin/env bash
set -e

cmd=$1
rest=${@:2}

bash -c "$ADR_SHELL_PATH $cmd $rest"

# Update docs
cat ./templates/SUMMARY.md > ./docs/SUMMARY.md
cat ./templates/adr/index.md > ./docs/adr/index.md

records=""
for file in ./docs/adr/*-*.md
do
  if [[ -f $file ]]; then
    name=$( echo $file | sed s/.*\\/// | sed s/\\.md//)
  
    link="[$name](./adr/${name}.md)";
    echo -e "  - $link" >> ./docs/SUMMARY.md
    
    link="[$name](./${name}.md)";
    records=$(echo -e "$records\n- $link");
  fi
done

echo -e "$records" >> ./docs/adr/index.md
