# 2. WebRTC communication protocol

Date: 2020-09-02

## Status

Proposed

## Context

The communication between every user of the service will be
critical. This communication must be done in real time, with as
small impact on infrastructure as possible and without having as
few touch points as possible with the backend.

The security of these communications must be absolute and therefore, the WebRTC technology is proposed here.

With such a technology, we can offer several medium to interact
with each other, depending on constraints of the service. For
example, the basic communication will text over a DataChannel;
but we can _easily_ extend this communication to audio and video
if necessary without changing anything on the core technology of
the application.

[1] https://en.wikipedia.org/wiki/WebRTC

### Connection establishment protocol

We are following the steps of this article: [Perfect Negociation
in WebRTC](https://blog.mozilla.org/webrtc/perfect-negotiation-in-webrtc/),
from [Jan-Ivar Bruaroey](https://blog.mozilla.org/webrtc/author/jbruaroeymozilla-com/)
on mozilla.org.

In every 2 peers communication, we have 3 actors: Alice, Bernard
and the Backend.

1. **init**: `Alice` and `Bernard` are retrieving ICE Servers from the `Backend`.
2. **init**: `Alice` and `Bernard` are opening a WebSocket connection to the `Backend`.
3. **createRTC**: `Alice` wants to connect to `Bernard` and create the _RTCPeerConnection_.
   1. `Alice` waits her ICE Candidates.
   2. `Alice` waits the _DataChannel_ to be ready.
4. **initiateSignaling**: `Alice` initiates the signaling to `Bernard`.
   1. `Alice` creates the _DataChannel_ on the `RTCPeerConnection` and waits the channel to be open.
5. **initiateSignaling**: `Alice` creates an _Offer_.
6. **initiateSignaling**: `Alice` defines her _LocalDescription_ with this _Offer_.
   1. Setting `LocalDescription` is starting a process to gather *ICECandidate*s from _ICE Servers_ for `Alice` to be contacted.
   2. Once a _ICECandidate_ is found, the _RTCPeerConnection_._onicecandidate_ callback is triggered.
   3. `Alice` sends the candidate for `Bernard` to the `Backend` over _WebSocket_.
   4. `Backend` receives the _ICECandidate_ from `Alice` and sends it back to `Bernard`.
7. **initiateSignaling**: `Alice` sends the _Offer_ to the `Backend`.
8. `Backend` receives the _Offer_ from `Alice` to `Bernard` from _WebSocket_.
9. `Backend` sends the _Offer_ to `Bernard` over _WebSocket_.
10. **onReceiveOfferOrAnswer**: `Bernard` receives the `Alice`'s _Offer_ from the `Backend` over _WebSocket_.
11. **onReceiveOfferOrAnswer**: `Bernard` creates a _RTCPeerConnection_ for `Alice`.
12. **onReceiveOfferOrAnswer**: `Bernard` defines `Alice`'s _RemoteDescription_ from the _Offer_.
13. **onReceiveOfferOrAnswer**: `Bernard` creates an _Answer_ to the `Alice`'s _Offer_.
14. **onReceiveOfferOrAnswer**: `Bernard` defines his _LocalDescription_ with this _Answer_.
15. **onReceiveOfferOrAnswer**: `Bernard` sends his _Answer_ back to `Alice` through the `Backend` over _WebSocket_.
16. `Backend` receives the _Answer_ from `Bernard` to `Alice` over _WebSocket_.
17. `Backend` sends the _Answer_ to `Alice` over _WebSocket_.
18. **onReceiveOfferOrAnswer**: `Alice` receives the `Bernard`'s _Answer_ from the `Backend` to her _Offer_ over _WebSocket_.
19. **onReceiveOfferOrAnswer**: `Alice` defines `Bernard`'s _RemoteDescription_ from the _Answer_.
20.
21. **onReceiveIceCandidate**: `Bernard` receives from the `Backend` the `Alice`'s _ICECandidate_.
22. **onReceiveIceCandidate**: `Bernard` adds the _ICECandidate_ to the _RTCPeerConnection_ candidates list.

## Decision

The change that we're proposing or have agreed to implement.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
