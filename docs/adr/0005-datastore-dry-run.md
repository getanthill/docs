# 5. datastore-dry-run

Date: 2020-11-21

## Status

Proposed

## Context

In the context of data validation, we need to check that a data is complying a
model schema. For this purpose, we need to add a `dry-run` route.

## Decision

The `dry-run` route will be activated with an additional header field:
`Dry-Run=true`. Its default value is `false`. Not all routes can support the
`Dry-Run` header but the following ones are potential first candidates:


| Action | Route |
| --: | :-- |
| `create` | `/api/m/:model/` |
| `update` | `/api/m/:model/:correlation_id` |
| `apply` | `/api/m/:model/:correlation_id/:event_type/:event_version` |
| `restore` | `/api/m/:model/:correlation_id/v/:version/restore` |
| `createSnapshot` | `/api/m/:model/:correlation_id` |

## Consequences

On these routes a new header will be added in the route contract to support this
new dry run mode having no effect on the database itself but playing the full
data alteration process.
