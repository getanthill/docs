# 3. @getanthill-npm-scope

Date: 2020-11-03

## Status

Accepted

## Context

We need to use a single scope for all Javascript libraries developed and
deployed with [npm](https://www.npmjs.com/).

## Decision

Starting now, we are using the scope `@getanthill` for all Javascript libraries
and services.

Packages from scope `@getanthill` are available here:
- https://www.npmjs.com/org/getanthill

## Consequences

### Publication

The build process must now contain a publication on `npm` and follow the
[`semver`](https://semver.org/) versioning format.

```shell
# Working on master
git checkout master
git pull --rebase

# Tag creation
git tag -a 0.1.1 -m "Creating version 0.1.1"
git push --tags

# Package publication
npm publish
```

### Install

To install a library from this scope, you can use the standard way
`npm i -S @getanhill/com` for example.

### Development

The easiest way to work with development version of libraries is to use
symlinks. Within your project:

```shell
rm -Rf ./node_modes/@getanthill
ln -s /path/to/development/getanthill ./node_modules/@getanthill
```
