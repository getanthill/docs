# Architecture Decision Record

`getanthill` Architecture Decision Record

## Changes management

> Every Record must be validated with a Pull Request

We are using [ADR](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)
to follow all our architecture decisions with the support of the
[adr-tools](https://github.com/npryce/adr-tools#adr-tools).

If you want to create a new entry, create a branch, execute the 
following command:

```shell
# Standard ADR
npm run adr -- new My new ADR

# Supercedes a previous one, use the -s option
npm run adr -- new -s 9 Use Rust for performance
```

This will create a new file in the folder [./docs/adr/](./docs/adr). Then open
the new file generated and start sharing your thoughts, analyses, pros and cons
in order to start the discussion about this structural change.

## Installation

Download and install [adr-tools](https://github.com/npryce/adr-tools#adr-tools)
then define in your environment the `ADR_SHELL_PATH`:

```shell
# .bashrc or .zshrc
export ADR_SHELL_PATH="~/path/to/adr-tools/src/adr";
```

## Serve

This project is using [mdbook](https://github.com/rust-lang/mdBook)
to serve and deploy the docs. Just download and add the `mdbook`
binary to your PATH to run the project locally with:

```shell
npm start
```

## Records

- [0001-record-architecture-decisions](./0001-record-architecture-decisions.md)
- [0002-webrtc-communication-protocol](./0002-webrtc-communication-protocol.md)
- [0003-getanthill-npm-scope](./0003-getanthill-npm-scope.md)
- [0004-getanthill-telemetry](./0004-getanthill-telemetry.md)
- [0005-datastore-dry-run](./0005-datastore-dry-run.md)
- [0006-deploy-sourcegraph](./0006-deploy-sourcegraph.md)
