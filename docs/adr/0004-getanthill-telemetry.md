# 4. @getanthill-tracer

Date: 2020-11-03

## Status

Accepted

## Context

We have to track usage, issues, bugs and exceptions everywhere on our stack
and with as much correlation as possible. Several tools or systems are
available to manage parts of this:

- https://sentry.io/
- https://opentelemetry.io/

## Decision

We must create a library `@getanthill/telemetry` abstracting these concepts
of *tracing* and *metrics*. This library must be built for both contexts:
backend and frontend (with possible limitation). Configuration must be
straightforward and performed from either injection or default environment
variables change.

## Consequences

Project `@getanthill/telementry` will be created:
- https://gitlab.com/getanthill/telemetry

## Ressources

- https://opentelemetry.io/docs/
- https://github.com/open-telemetry/opentelemetry-js
- [Getting Started `opentelemetry-js`](https://github.com/open-telemetry/opentelemetry-js/tree/master/getting-started#getting-started-with-opentelemetry-js)
- https://docs.telemetry.mozilla.org/introduction.html
