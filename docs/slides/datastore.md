<!-- 

Plan:

- Purpose
- Features
- Use Cases
- Installation
- Deployment

 -->

# Datastore

<footer>
  <a href="https://gitlab.com/getanthill/datastore/-/blob/master/docs/README.md#documentation">https://gitlab.com/getanthill/datastore</a>
</footer>

//

///

## Purpose

//

*Solve the problem of the Data Access*

//

### Unified Data Model

<div class="super-gray">
  Single Source of Truth, Inventory, Documentation,

</div>

//

### 100% Event-Sourced

<div class="super-gray">
  Track Everything, Time Travel, Concurrency
</div>

//

### Central Access

<div class="super-gray">
  Monitor, xBAC
</div>

//

### Data Privacy

<div class="super-gray">
  GDPR compliant, Personal Data tagging,<br/>
  Automatic Deletion
</div>

//

### Flexible Deployment Strategies

<ul>
  <li class="fragment"><strong>One</strong> Central Access</li>
  <li class="fragment"><strong>One</strong> Sidecar per Microservice</li>
  <li class="fragment"><strong>One</strong> Deployment per Domain</li>
</ul>
  





///

## Manage





///

## Features

//

### Event-Sourced

//

### Open-API 3.0 Compliant

<footer>
  <a href="https://www.openapis.org/">https://www.openapis.org</a>
</footer>

//

### GraphQL

<footer>
  <a href="https://graphql.org/">https://graphql.org</a>
</footer>

//

### Data Model History

//

### Entities Relationships Graph

//

### Design By Contract

//

### SDK





///

## Use Cases

//

### Event-Sourced Data Service

//

### Data Science

Query your Data based on strict model features definition
