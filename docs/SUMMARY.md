# Architecture Decision Record

[Index](./index.md)
[Styleguide](./styleguide/index.md)
[Mentorings](./mentorings/index.md)
[Architecture Decision Records](./adr/index.md)
  - [0001-record-architecture-decisions](./adr/0001-record-architecture-decisions.md)
  - [0002-webrtc-communication-protocol](./adr/0002-webrtc-communication-protocol.md)
  - [0003-getanthill-npm-scope](./adr/0003-getanthill-npm-scope.md)
  - [0004-getanthill-telemetry](./adr/0004-getanthill-telemetry.md)
  - [0005-datastore-dry-run](./adr/0005-datastore-dry-run.md)
  - [0006-deploy-sourcegraph](./adr/0006-deploy-sourcegraph.md)
