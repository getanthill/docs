mermaid.initialize({
  startOnLoad:true,
  themeCSS: `
    .node rect {
      fill: var(--fg);
      stroke: var(--bg);
    }

    .edgePath .path,
    .edgePath path {
      stroke: var(--fg);
      fill: var(--fg);
    }

    .node .label {
      color: var(--bg);
    }
  `,
  flowchart:{
    useMaxWidth:true,
    htmlLabels:true,
    curve:'cardinal',
  },
});

window.MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  },
  svg: {
    fontCache: 'global'
  }
};
